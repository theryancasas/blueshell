/**
 * 
 *	Project Name:	BlueShell
 *	Author: 		Ryan Casas
 *  Contact:		ryan@uma.es
 *	Made in:		UMA 2014/2015
 *	Subject:		Operative Systems
 * 
 * 	BlueShell meets the minimal requirements asked by the UMA's Computing Architecture Department.
 * 	
 * 	What can be done with BlueShell:
 * 		· Execute any program available in the system PATH (such as "ls", "pwd", etc.)
 * 		· Add & to the end of any command line to execute it in background (example "xeyes &")
 * 		· Interrupt a foreground program with ^C
 * 		· Freeze / Suspend a foreground program with ^Z
 * 		· Internal "cd" command to change Current Directory
 * 		· Internal "jobs" command to list background processes
 * 		· Internal "history" command to list already used commands
 * 		· Use "help" to know what more you can do with BlueShell
 * 
 *	To compile and run BlueShell:
 *	$ gcc Shell_project.c job_control.c blueshell.c -o BlueShell; ./BlueShell
 * 
**/

#define MAX_LINE 256 /* 256 chars per line, per command, should be enough. */

#include "job_control.h"

job* jobs; /* Job struct used to store processes in background/stopped */
job* history; /* Job struct used to store every command the user inputs */
struct termios conf; /* Struct used to store the default terminal configuration */
int shell_terminal; /* ID del fichero Terminal */

#include "blueshell.h"
#include <string.h>

/* Custom SIGCHILD handler */
void sigchld_handler(int signum){
    int i, _status, info, pid_wait;
    enum status status_res;
    job* jb, aux;
    
	printf("\x1B[0mSIGCHLD received\n");
	for(i = 1; i<=list_size(jobs); i++){
        jb = get_item_bypos(jobs, i);
        pid_wait = waitpid(jb->pgid, &_status, WUNTRACED | WNOHANG);
                
        if(pid_wait == jb->pgid){
            status_res = analyze_status(_status, &info);
            printf("%s#%i received signal (Status: %s)\n", jb->command, jb->pgid, status_strings[status_res]);
            block_SIGCHLD();
				if(status_res == SUSPENDED)	edit_job(jobs, jb, STOPPED);
				else if (status_res == EXITED || status_res == SIGNALED){
					delete_job(jobs, jb);
					i--;
				}
            unblock_SIGCHLD();
            
            tcsetattr(shell_terminal, TCSANOW, &conf);
           }
        }
    
    fflush(stdout);
    return;
}


/* --------------------------	MAIN	-------------------------*/

int main(void)
{
	char inputBuffer[MAX_LINE];	/* buffer to hold the command entered */
	int background;				/* equals 1 if a command is followed by '&' */
	char *args[MAX_LINE/2];		/* command line (of 256) has max of 128 arguments */
	int pid_fork, pid_wait;		/* pid for created and waited process */
	int status;					/* status returned by wait */
	enum status status_res;		/* status processed by analyze_status() */
	int info;					/* info processed by analyze_status() */
	job* auxJob;				/* stores a temporal job */
	char* str;					/* stores a temporal string */
	int i;						/* traditional iterator integer variable */
	
	/* Initialize lists */
	jobs = new_list("jobs"); 
	history = new_list("history");
	
	/* Get the terminal ID, then get its configuration */
	shell_terminal = STDIN_FILENO;
	tcgetattr(shell_terminal, &conf);

	/* Assign SIGCHLD handler */
	signal(SIGCHLD, sigchld_handler);
	
	/* Print the logo */
	printf("\x1B[36m____    ¦¦¦     ¦    ¦¦ ¦¦¦¦¦¦            ¦¦¦¦¦¦  ¦¦¦ ¦¦ ¦¦¦¦¦¦  ¦¦¦     ¦¦¦    \n¦¦¦¦¦¦_ ¦¦¦¦     ¦¦  ¦¦¦¦¦¦   ¯          ¦¦¦    ¦ ¦¦¦¦ ¦¦¦¦¦   ¯ ¦¦¦¦    ¦¦¦¦    \n¦¦¦¦ _¦¦¦¦¦¦    ¦¦¦  ¦¦¦¦¦¦¦¦            ¦ ¦¦¦_   ¦¦¦¯¯¦¦¦¦¦¦¦   ¦¦¦¦    ¦¦¦¦    \n¦¦¦¦¦¯  ¦¦¦¦    ¦¦¦  ¦¦¦¦¦¦¦  _            ¦   ¦¦¦¦¦¦ ¦¦¦ ¦¦¦  _ ¦¦¦¦    ¦¦¦¦    \n¦¦¦  ¯¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦ ¦¦¦¦¦¦¦         ¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦\n¦¦¦¦¦¦¯¦¦\x1B[0m ¦¦¦  ¦¦¦¦¦ ¦ ¦ ¦¦ ¦¦ ¦         ¦ ¦¦¦ ¦ ¦ ¦ ¦¦¦¦¦¦¦ ¦¦ ¦¦ ¦¦¦  ¦¦ ¦¦¦  ¦\n¦¦¦   ¦ ¦ ¦ ¦  ¦¦¦¦¦ ¦ ¦  ¦ ¦  ¦         ¦ ¦¦  ¦ ¦ ¦ ¦¦¦ ¦ ¦ ¦  ¦¦ ¦ ¦  ¦¦ ¦ ¦  ¦\n ¦    ¦   ¦ ¦    ¦¦¦ ¦ ¦    ¦            ¦  ¦  ¦   ¦  ¦¦ ¦   ¦     ¦ ¦     ¦ ¦   \n ¦          ¦  ¦   ¦        ¦  ¦               ¦   ¦  ¦  ¦   ¦  ¦    ¦  ¦    ¦  ¦\n      ¦\n\n");
	printf("v1.1725 by Ryan Casas\nType ''help'' to list the commands available\n");
	
	while (1){ /* Program terminates normally inside get_command() after ^D is typed*/
		ignore_terminal_signals(); /* Remind our custom shell to avoid being interrupted by ^C or ^Z */
		
		printf("\x1B[36mBlueShell ?\x1B[0m");
		fflush(stdout);
		
		get_command(inputBuffer, MAX_LINE, args, &background); /* Wait for a command to be inputted */
		
		
		if(args[0]==NULL) continue; /* Nothing has been written, let's wait for another command */
		
		if(strcmp(args[0], "help") == 0){
			/* Output basic shell help */
			
			printf("\x1B[36m");
				printf("\nHelp is here to help!\n");
				printf("\n\x1B[0m	· help			\x1B[36mlist these commands");
				printf("\n\x1B[0m	· quit			\x1B[36msame as exit");
				printf("\n\x1B[0m	· exit			\x1B[36msame as quit");
				printf("\n\x1B[0m	· history		\x1B[36mlists all the commands that you have introduced in this shell");
				printf("\n\x1B[0m	· history [id]		\x1B[36mreruns the command in the position id");
				printf("\n\x1B[0m	· jobs			\x1B[36mlists all the currently existing background processes (invoked by this shell)");
				printf("\n\x1B[0m	· cd			\x1B[36mchanges the working directory to your HOME directory");
				printf("\n\x1B[0m	· cd <path>		\x1B[36mchanges the working directory (type pwd to know the current one)");
				printf("\n\x1B[0m	· bg [process]		\x1B[36mput a process in background. If no number is given, it will take the latest one");
				printf("\n\x1B[0m	· fg [process]		\x1B[36mput a process in foreground. If no number is given, it will take the latest one");
			printf("\n\n\x1B[0m");
		}else if(strcmp(args[0], "quit") == 0 || strcmp(args[0], "exit") == 0){
			/* exit the shell */
			printf("\x1B[36mExiting BlueShell...\x1B[0m\n");
			exit(0);
		}else if(strcmp(args[0], "history") == 0 || strcmp(args[0], "historial") == 0){
			if(args[1] == NULL){
				/* prints the history list */
				
				printf("\x1B[36m");
					print_job_list(history);
				printf("\x1B[0m");
			}else{
				/* Executes the job demanded */
				auxJob = get_item_bypos(history, atoi(args[1]));
				if(auxJob != NULL){
					
					args[1] = NULL; /* We need to reset the argument part to avoid problems with argument-less programs */
					
					/* Split the command and assign arguments before executing it again */
					i = 0;
					str = strtok(auxJob->command, " ");
					while (str != NULL){
						args[i] = str;
						str = strtok (NULL, " ");
						i++;
					}
					
					background = (auxJob->state == BACKGROUND);
					
					goto execute;
				}else{
					printf("\n\x1B[31m Whoops! Entry #%i not found in the history \x1B[0m\n", atoi(args[1]));
				}
			}
			continue;
		}else if(strcmp(args[0], "cd") == 0){
			/* modify current working directory */
			chdir(args[1]==NULL? getenv("HOME") : args[1]);
		}else if(strcmp(args[0], "fg") == 0){
			/* Put a Job in foreground */
			
			if(!empty_list(jobs)){ /* check if there is any job */
				block_SIGCHLD();
					auxJob = get_item_bypos(jobs, (args[1] != NULL)? atoi(args[1]) : 1);
				unblock_SIGCHLD();
				
				if(auxJob != NULL){
					execute_background(auxJob);
					
					block_SIGCHLD();
						edit_job(jobs, auxJob, FOREGROUND);
					unblock_SIGCHLD();
					
					execute_foreground(auxJob->pgid, getpid(), jobs);
					tcsetattr(shell_terminal, TCSANOW, &conf);
				}else{
					printf("\n\x1B[31m Whoops! Entry #%i not found in the job list \x1B[0m\n", atoi(args[1]));
				}
				
			}else{
				printf("\n\x1B[31mThere is not any job available\x1B[0m\n");
			}
			continue;
		}else if(strcmp(args[0], "bg") == 0){
			/* sends a SIGCONT to a job to put it on background */
			
			if(!empty_list(jobs)){
				block_SIGCHLD();
					auxJob = get_item_bypos(jobs, (args[1] != NULL)? atoi(args[1]) : 1);
				unblock_SIGCHLD();
				if(auxJob != NULL){
					execute_background(auxJob);
				}else{
					printf("\n\x1B[31m Whoops! Entry #%i not found in the job list \x1B[0m\n", atoi(args[1]));
				}
			}else{
				printf("\n\x1B[31mThere is not any job available\x1B[0m\n");
			}
		}else if(strcmp(args[0], "jobs") == 0){
			printf("\x1B[36m");
				print_job_list(jobs);
			printf("\x1B[0m");
		}else{
			execute: /* Execute command */

			pid_fork = fork();
			switch(pid_fork){
				case -1:
					printf("\n\x1B[31mError creating a child... This action will be reported.\x1B[0m\n");
					exit(EXIT_FAILURE);
				break;
				
				case 0: /* Child process */
					new_process_group(getpid());
					restore_terminal_signals(); /* Same as terminal_signals(SIG_DFL) */
					
					printf("\x1B[0m"); /* Force the child to paint the color by default */
					execvp(args[0], args); /* Transform the child process to the process we requested */
					
					printf("\n\x1B[31mError creating a child: ''%s'' not found \x1B[0m\n", args[0]);
					exit(EXIT_FAILURE);
				break;
				
				default: /* Father process */
					implode(inputBuffer, MAX_LINE, " ", args);
					
					add_job(history, new_job(pid_fork, inputBuffer, background?BACKGROUND:FOREGROUND));
					block_SIGCHLD();
						add_job(jobs, new_job(pid_fork, args[0], background?BACKGROUND:FOREGROUND));
					unblock_SIGCHLD();
						
					if(!background){ /* Foreground */
						execute_foreground(pid_fork, getpid(), jobs);
						tcsetattr(shell_terminal, TCSANOW, &conf);
					}else{
						status_res = analyze_status(status, &info);
						printf("\n\x1B[36mProcess %s#%i running on background (Status: %s)\x1B[0m\n", args[0], pid_fork, status_strings[status_res] );
					}
				break;
			}
		}

	} /* While END */
}