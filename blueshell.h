/**
 * 
 *	Project Name:	BlueShell
 *	Author: 		Ryan Casas
 *  Contact:		ryan@uma.es
 *	Made in:		UMA 2014/2015
 *	Subject:		Operative Systems
 * 
 * 	This file defines functions used in the shell and not present in the
 * 	original source files.
 * 
**/

#ifndef _BLUESHELL_H
#define _BLUESHELL_H
#include "job_control.h"

/* Find a job "item" in a jobList "list" and modifies its state to "state" */
int edit_job(job * list, job * item, enum job_state state);

/* controls an already executed process#childpid */
void execute_foreground(int childpid, int parentpid, job * jobs);

/* sends a SIGCONT to process auxJob->pgid to put it on background */
void execute_background(job* auxJob);

/* collapses an array to a string using a delimiter (delim). Useful with strtok */
size_t implode(char *out_string, size_t out_bufsz, const char *delim, char **chararr);

#endif