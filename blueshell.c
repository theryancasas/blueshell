/**
 * 
 *	Project Name:	BlueShell
 *	Author: 		Ryan Casas
 *  Contact:		ryan@uma.es
 *	Made in:		UMA 2014/2015
 *	Subject:		Operative Systems
 * 
 * 	This file implements functions used in the shell and not present in
 * 	the original source files.
 * 
**/

#include "blueshell.h"

int edit_job(job * list, job * item, enum job_state state){
	job * aux = list;
	while(aux->next != NULL && aux->pgid != item->pgid){
		aux = aux->next;
	}
	
	if(aux->pgid == item->pgid){
		aux->state = state;
		return 1;
	}return 0;
}

void execute_foreground(int childpid, int parentpid, job * jobs){
	int _status, info;
	enum status status_res;
	job * auxJob;
	
	auxJob = get_item_bypid(jobs, childpid);
	
	printf("\n\x1B[36mProcess %s#%i running on foreground (Status: %s)\x1B[0m\n", auxJob->command, childpid, status_strings[status_res] );
	
	set_terminal(childpid); /* Set terminal to the child */
	waitpid(childpid, &_status, WUNTRACED); /* Waits until process is signaled */
	set_terminal(parentpid); /* Set terminal to the parent */
	
	printf("\n");
					
	status_res = analyze_status(_status, &info);
					
	block_SIGCHLD();
		edit_job(jobs, auxJob, STOPPED); /* Update the job status to STOPPED */
		printf("\x1B[36mProcess %s#%i has stopped (Status: %s)\x1B[0m\n", auxJob->command, childpid, status_strings[status_res]);
		
		/* We check if the job is out, and we delete it from the job list */
		if(status_res == EXITED || status_res == SIGNALED)	delete_job(jobs, auxJob);
		
	unblock_SIGCHLD();
	fflush(stdout);
}

void execute_background(job* auxJob){
	if(auxJob->state == STOPPED) killpg(auxJob->pgid, SIGCONT);
}

static char *util_cat(char *dest, char *end, const char *str){
    while (dest < end && *str)	*dest++ = *str++;
    return dest;
}

size_t implode(char *out_string, size_t out_buffersize, const char *delimiter, char **chararray){
    char *ptr = out_string;
    char *strend = out_string + out_buffersize;
    while (ptr < strend && *chararray){
        ptr = util_cat(ptr, strend, *chararray);
        chararray++;
        if (*chararray)	ptr = util_cat(ptr, strend, delimiter);
    }
    return ptr - out_string;
}